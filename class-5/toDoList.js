// If an li element is clicked, toggle the class "done" on the <li>

const toggleDone = function(e) {
  const liEl = document.getElementsByTagName('li')[i];
  liEl.setAttribute(
  'class',
  'done'
  );
}

const toDoList = document.querySelectorAll('li');
for (let i = 0; i < toDoList.length; i++) {
  toDoList[i].addEventListener('click', toggleDone);
}

// If a delete link is clicked, delete the li element / remove from the DOM

const deleteItem = function(e) {
  const deleteElListItem = this.parentNode;
  const listParent = deleteElListItem.parentNode;
  listParent.removeChild(deleteElListItem);
  e.stopPropagation();
}

const deleteEls = document.getElementsByClassName('delete');;
for (let i = 0; i < deleteEls.length; i++) {
  deleteEls[i].addEventListener('click', deleteItem);
}

// If a "Move to..."" link is clicked, it should move the item to the correct
// list.  Should also update the working (i.e. from Move to Later to Move to Today)
// and should update the class for that link.
// Should *NOT* change the done class on the <li>

const moveItem = function(e) {
  if (this.classList.contains('toLater')) {
    const moveLaterListItem = this.parentNode;
    const laterListParent = moveLaterListItem.parentNode;
    laterListParent.removeChild(moveLaterListItem);

    const laterList = document.getElementsByClassName('later-list');
    laterList[0].appendChild(moveLaterListItem);

    this.setAttribute(
    'class',
    'move toToday'
    );

    this.innerHTML = 'Move to Today';
    e.stopPropagation();
  } else {
    const moveTodayListItem = this.parentNode;
    const todayListParent = moveTodayListItem.parentNode;
    todayListParent.removeChild(moveTodayListItem);

    const todayList = document.getElementsByClassName('today-list');
    todayList[0].appendChild(moveTodayListItem);

    this.setAttribute(
    'class',
    'move toLater'
    );

    this.innerHTML = 'Move to Later';
    e.stopPropagation();
  }
}

const moveEls = document.getElementsByClassName('move');
for (let i = 0; i < moveEls.length; i++) {
  moveEls[i].addEventListener('click', moveItem);
}

// If an 'Add' link is clicked, adds the item as a new list item in correct list
// addListItem function has been started to help you get going!
// Make sure to add an event listener to your new <li> (if needed)
const addListItem = function(e) {
  e.preventDefault();
  const input = this.parentNode.getElementsByTagName('input')[0];
  const text = input.value; // use this text to create a new <li>

  const parentSection = this.parentNode.parentNode;
  const parentList = parentSection.getElementsByTagName('ul')[0];

  // Create li tag and span with a textNode containing the user input
  const liItem = document.createElement('li');
  liItem.addEventListener('click', toggleDone);

  const spanEl = document.createElement('span');
  spanEl.appendChild(document.createTextNode(text));
  liItem.appendChild(spanEl);

  // if the parent section is 'Today', attach the relevant elements & text
  if (parentSection.classList.contains('today')) {
    const moveLaterATag = document.createElement('a');
    moveLaterATag.setAttribute(
    'class',
    'move toLater'
    );
    moveLaterATag.innerHTML = 'Move to Later';
    liItem.appendChild(moveLaterATag);
    moveLaterATag.addEventListener('click', moveItem);
  } else { // else, attach the opposite elements & text
    const moveTodayATag = document.createElement('a');
    moveTodayATag.setAttribute(
    'class',
    'move toToday'
    );
    moveTodayATag.innerHTML = 'Move to Today';
    liItem.appendChild(moveTodayATag);
    moveTodayATag.addEventListener('click', moveItem);
  }

  // attach the delete element
  const deleteATag = document.createElement('a');
  deleteATag.setAttribute(
    'class',
    'delete'
  );
  deleteATag.innerHTML = 'Delete';
  liItem.appendChild(deleteATag);
  deleteATag.addEventListener('click', deleteItem);

  parentList.appendChild(liItem);

}

// Add this as a listener to the two Add links
const addEls = document.getElementsByClassName('add-item');
for (let i = 0; i < addEls.length; i++) {
  addEls[i].addEventListener('click', addListItem);
}
