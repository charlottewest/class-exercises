// Traversing the DOM
/**
  * 1. body.firstChild
  * 2. ul.parentNode.parentNode
  * 3. p.previousSibling.lastChild
 */


// Product CRUD
// Create a new <a> element containing the text "Buy Now!"
// with an id of "cta" after the last <p>

const mainEl = document.getElementsByTagName('main')[0];
console.log(mainEl.lastChild);

const aTag = document.createElement('a');
const aTextNode = document.createTextNode("Buy Now");
aTag.appendChild(aTextNode);

const paragraph = document.getElementsByTagName('p')[0];
console.log(paragraph);
mainEl.insertBefore(aTag, paragraph);

// Access (read) the data-color attribute of the <img>,
// log to the console
const carImg = document.getElementsByTagName('img')[0];
const colorAttr = carImg.dataset.color;
console.log(colorAttr);

// Update the third <li> item ("Turbocharged"),
// set the class name to "highlight"

const thirdLi = document.getElementsByTagName('li')[2];
console.log(thirdLi);
thirdLi.setAttribute(
'class',
'highlight'
);

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")
mainEl.removeChild(paragraph);

// Pluses and Minuses

let counter = 0;
const plus = document.getElementById('plus');
plus.addEventListener('click', function() {
  counter++;
  document.getElementById('count').innerHTML = counter;
});

const minus = document.getElementById('minus');
minus.addEventListener('click', function() {
  counter--;
  document.getElementById('count').innerHTML = counter;
});
