/**
 * Determines whether meat temperature is high enough
 * @param {string} kind
 * @param {number} internalTemp
 * @param {string} doneness
 * @returns {boolean} isCooked
 */
const foodIsCooked = function(kind, internalTemp, doneness) {
  if (kind === 'chicken' && internalTemp >= 165) {
      return true;
  } else if (kind === 'beef' && internalTemp >= 125 && doneness === 'rare') {
      return true;
  } else if (kind === 'beef' && internalTemp >= 135 && doneness === 'medium') {
      return true;
  } else if (kind === 'beef' && internalTemp >= 155 && doneness === 'well') {
      return true;
  } else {
      return false;
  }
};


// Test function
console.log(foodIsCooked('chicken', 90)); // should be false
console.log(foodIsCooked('chicken', 190)); // should be true
console.log(foodIsCooked('beef', 138, 'well')); // should be false
console.log(foodIsCooked('beef', 138, 'medium')); // should be true
console.log(foodIsCooked('beef', 138, 'rare')); // should be true




/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const getDeck = function() {
  let cards = [];
  const suits = ['hearts', 'spades', 'clubs', 'diamonds'];

  for(let i = 0; i < suits.length; i++) {
    for(let j = 2; j <= 10; j++) {
      cards.push({
        val: j,
        displayVal: j.toString(),
        suit: suits[i],
      });
    }
    cards.push({
      val: 10,
      displayVal: 'King',
      suit: suits[i],
    });
    cards.push({
      val: 10,
      displayVal: 'Queen',
      suit: suits[i],
    });
    cards.push({
      val: 10,
      displayVal: 'Jack',
      suit: suits[i],
    });
    cards.push({
      val: 11,
      displayVal: 'Ace',
      suit: suits[i],
    });
  }
  return cards;
}

// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
    randomCard.displayVal &&
    typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);
