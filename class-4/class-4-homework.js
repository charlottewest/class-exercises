const getDeck = function() {
  let cards = [];
  const suits = ['hearts', 'spades', 'clubs', 'diamonds'];

  for(let i = 0; i < suits.length; i++) {
    for(let j = 2; j <= 10; j++) {
      cards.push({
        val: j,
        displayVal: j.toString(),
        suit: suits[i],
      });
    }
    cards.push({
      val: 10,
      displayVal: 'King',
      suit: suits[i],
    });
    cards.push({
      val: 10,
      displayVal: 'Queen',
      suit: suits[i],
    });
    cards.push({
      val: 10,
      displayVal: 'Jack',
      suit: suits[i],
    });
    cards.push({
      val: 11,
      displayVal: 'Ace',
      suit: suits[i],
    });
  }
  return cards;
}
const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */


const CardPlayer = function CardPlayer(name) {
  this.name = name;
  this.hand = [];
  this.drawCard = function() {
    const randomCard = blackjackDeck[Math.floor(Math.random() * 52)];
    this.hand.push(randomCard);
  };
  this.drawCard();
};

// CREATE TWO NEW CardPlayers
let dealer = new CardPlayer('dealer');
let player = new CardPlayer('player');

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} - Object containing Total points and whether hand isSoft
 */

let testHands = [{val: 11, displayVal: "Ace", suit: "hearts"}, {val: 3, displayVal: "3", suit: "spades"}];
// console.log(testHands.length);
const calcPoints = function(hand) {
  const valArray = hand.map(function (card) {
    return card.val
  });
  let total = valArray.reduce((a, b) => a + b, 0);
  let isSoft = false;

  const match = 11;
  let count = valArray.reduce(function(n, val) {
    return n + (val === match);
  }, 0);
  // console.log(count);

  if (count > 1) {
    indexOfEleven = valArray.indexOf(11);
    valArray[indexOfEleven] = 1;
    // console.log(valArray);
    isSoft = true;
    total = valArray.reduce((a, b) => a + b, 0);
  } else if (count === 1 ) {
    if (total > 21) {
      indexOfEleven = valArray.indexOf(11);
      valArray[indexOfEleven] = 1;
      total = valArray.reduce((a, b) => a + b, 0);
    } else {
      isSoft = true;
    }
  }
  return [total, isSoft];

};
// calcPoints(testHands);
console.log(calcPoints(dealer.hand));
console.log(calcPoints(player.hand));

/**
 * Determines whether the dealer should draw another card
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = function(dealerHand) {
  let dealerTotal = calcPoints(dealerHand)[0];
  let isSoft = calcPoints(dealerHand)[1];

  if (dealerTotal <= 16) {
    return true;
  } else if (dealerTotal === 17 && isSoft === true) {
    return true;
  } else {
    return false;
  }
};
console.log(dealerShouldDraw(dealer.hand));
/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore
 * @param {number} dealerScore
 * @returns {string} States the player's score, the dealer's score, and who wins
 */
const determineWinner = function(playerScore, dealerScore) {
  console.log(playerScore);
  console.log(dealerScore);
  if (playerScore > dealerScore) {
    winner = 'player';
  } else {
    winner = 'dealer';
  }

  return `Player score: ${playerScore}. Dealer score: ${dealerScore} Winner is ${winner}.`
}

console.log(determineWinner(calcPoints(player.hand)[0], calcPoints(dealer.hand)[0]))
/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count
 * @param {string} dealerCard
 */
const getMessage = function(count, dealerCard) {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player
 */
const showHand = function(player) {
  let displayHand = player.hand.map(function(card) { return card.displayVal});
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand)[0];
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  // IMPLEMENT DEALER LOGIC BELOW

  let dealerScore = calcPoints(dealer.hand)[0];
  showHand(dealer);
  while (dealerShouldDraw === true) {
    dealer.drawCard();
    dealerScore = calcPoints(player.hand)[0];
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Dealer stands at ${dealerScore}`);


  return determineWinner(playerScore, dealerScore);
}
console.log(startGame());
