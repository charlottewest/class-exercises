// Class 2 Exercises

// Create an object of yourself
const mySelf = {
  firstName: 'Charlotte',
  lastName: 'West',
  'favorite food': 'sushi',
  mom: {
    firstName: 'Petra',
    lastName: 'West',
    'favorite food': 'mashed potatoes'
  },
  cat: {
    firstName: 'Fish',
    lastName: 'West',
    'favorite food': 'fish'
  }
};

catFood = mySelf.cat.firstName
momName = mySelf.mom['favorite food']

console.log(`My cat's first name: ${catFood}` );
console.log(`My mom's favorite food: ${momName}` );


// Create an array for THIS tic tac toe board
let ticTacToe = ['-', 'O', '-', '-', 'X', 'O', 'X', '-', 'X'];
ticTacToe[2] = 'O';

console.log(ticTacToe.slice(0, 3));
console.log(ticTacToe.slice(3, 6));
console.log(ticTacToe.slice(6, 9));
