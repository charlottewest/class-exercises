const assignmentDate = '1/21/2019';

// Convert to a Date instance

const parsedDate = Date.parse(assignmentDate.padStart(10, '0'));
console.log(parsedDate); //1548057600000

// Create dueDate which is 7 days after assignmentDate

const dueDate = new Date(parsedDate + (7 * 24 * 60 * 60 * 1000));
console.log(dueDate); // Mon Jan 28 2019 00:00:00 GMT-0800 (Pacific Standard Time)

// Use dueDate values to create an HTML time tag in format
// <time datetime="YYYY-MM-DD">Month day, year</time>
// log this value using console.log

const dueDay = dueDate.getDate(); // 28

// If the month contains a single digit, pad with a 0,
// add 1 to account for month being an index, 0-11
const parseMonth = dueDate.getMonth()
if (parseMonth.toString().length == 1) {
  let dueMonth = parseMonth.toString().padStart(1, '0') + 1;
} else {
  let dueMonth = parseMonth + 1;
} // 01

const dueYear = dueDate.getFullYear(); // 2019

// Get strings to interpolate into the timeTag text
const monthString = dueDate.toString().split(' ')[1]; // Jan
const dayString = dueDate.toString().split(' ')[2]; // 28
const yearString = dueDate.toString().split(' ')[3]; // 2019

const timeStamp = `${dueYear}-${dueMonth}-${dueDay}`
const timeTag = `<time datetime="${timeStamp}">${monthString}, ${dayString} ${yearString}</time>`
console.log(timeTag);
