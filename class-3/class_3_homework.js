// Parse phone number

// You are given a phone number as a string, with one of the
// following formats (but you don't know which one!)
// '(206) 333-4444'
// '206-333-4444'
// '206 333 4444'

// Create a function testPhoneNumber that takes in a phoneNumber string
// in one of the above formats.  This should use a regular expression
// and run the test method to determine if the number is valid
// Returns the result of the test method call (either true or false)

const testPhoneNumber = function(phoneNumber) {
  const regex = /^[- (]*\d{3}[- )]*\d{3}[- ]*\d{4}$/;

  const testMatch = regex.test(phoneNumber);
  console.log(testMatch);
}

// Check testPhoneNumber
testPhoneNumber('206-333-4444');  // returns true
testPhoneNumber('206-12-3456');  // returns false



// Create a function parsePhoneNumber that takes in a phoneNumber string
// in one of the above formats.  For this, you can *assume the phone number
// passed in is correct*.  This should use a regular expression
// and run the exec method to capture the area code and remaining part of
// the phone number.
// Returns an object in the format {areaCode, phoneNumber}

const parsePhoneNumber = function(phoneNumber) {
  const regex = /^[- (]*(\d{3})[- )]*(\d{3}[- ]*\d{4})$/;
  const testMatch = regex.exec(phoneNumber);
  // I'm not sure why this is returning 3 regex groups instead of 2
  // but as is, testMatch[0] returns the full string
  console.log(`areaCode: ${testMatch[1]}, phoneNumber: ${testMatch[2]}`);
}

// Check parsePhoneNumber
parsePhoneNumber('206-333-4444');
// returns {areaCode: '206', phoneNumber: '3334444'}

parsePhoneNumber('(222) 422-5353');
// returns {areaCode: '222', phoneNumber: '4225353'}



// Soccer standings

(function() {
  const RESULT_VALUES = {
    w: 3,
    d: 1,
    l: 0
  }

  // This function accepts one argument, the result, which should be a string
  // Acceptable values are 'w', 'l', or 'd'
  const getPointsFromResult = function getPointsFromResult(result) {
    return RESULT_VALUES[result];
  }

  // Create getTotalPoints function which accepts a string of results
  // including wins, draws, and losses i.e. 'wwdlw'
  // Returns total number of points won

  const getTotalPoints = function(resultsString) {
    let total = 0;
    const resultsArray = resultsString.split('');

    resultsArray.forEach(function(result)  {
      pointValue = getPointsFromResult(result);
      total += pointValue;
    });
    return total;
  }


  // Check getTotalPoints
  console.log(getTotalPoints('wwdl')); // should equal 7

  // create orderTeams function that accepts as many team objects as desired,
  // each argument is a team object in the format { name, results }
  // i.e. {name: 'Sounders', results: 'wwlwdd'}
  // Logs each entry to the console as "Team name: points"

  const orderTeams = function() {
    let total = 0;
    const teams = Array.from(arguments);
    teams.forEach(function(team)  {
      totalPoints = getTotalPoints(team.results);
      teamName = team.name;
      console.log(`${teamName}: ${totalPoints}`);
    });
  };

  // Check orderTeams
  orderTeams(
    {name: 'Sounders', results: 'wwdl'},
    {name: 'Galaxy', results: 'wlld'}
  );

// should log the following to the console:
// Sounders: 7
// Galaxy: 4

})();
