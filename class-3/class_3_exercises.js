// Email Regex
const myEmail = 'foo@foo.com';
const regex = /^\w+@\w+\.[a-z]*$/;

const match = myEmail.match(regex);
console.log(match);


// Battle game

// Create attack function below.  This will take the following parameters:
// attackingPlayer, defendingPlayer, baseDamage, variableDamage

const attack = function(attackingPlayer, defendingPlayer, baseDamage, variableDamage) {
  let totalDamage = baseDamage + Math.floor(Math.random() * (variableDamage + 1));
  console.log(`${totalDamage}`)

  defendingPlayer.health = defendingPlayer.health - totalDamage;
  console.log(`${defendingPlayer.health}`)

  message = `${attackingPlayer.name} hits ${defendingPlayer.name} with ${totalDamage} damage`
  return(message);
}

// Create player1 and player2 objects below
// Each should have a name property of your choosing, and health property equal to 10

let player1 = {
  name: 'Inara',
  health: 10
};

let player2 = {
  name: 'Mal',
  health: 10
};

// DO NOT MODIFY THE CODE BELOW THIS LINE
// Set attacker and defender.  Reverse roles each iteration
let attackOrder = [player1, player2];

// Everything related to preventInfiniteLoop would not generally be necessary, just adding to
// safeguard students from accidentally creating an infinite loop & crashing browser
let preventInfiniteLoop = 100;
while (player1.health >= 1 && player2.health >= 1 && preventInfiniteLoop > 0) {
  const [attackingPlayer, defendingPlayer] = attackOrder;
  console.log(attack(attackingPlayer, defendingPlayer, 1, 2));
  attackOrder = attackOrder.reverse();

  preventInfiniteLoop--;
}
const eliminatedPlayer = player1.health <= 0 ? player1 : player2;
console.log(`${eliminatedPlayer.name} has been eliminated!`);


// Itemized Receipt

// create function logReceipt that accepts menu items (1 or many) as objects
// with these properties: {descr, price}
// i.e. {descr: 'Coke', price: 1.99}
// function should log each item to the console and log a total price

const logReceipt = function() {
  let total = 0;
  const items = Array.from(arguments);
  items.forEach(function(item)  {
    itemPrice = item.price;
    total += itemPrice;
    itemDescr = item.descr;
    console.log(`${itemDescr} - ${itemPrice}`);
  });
  console.log(`Total - ${total}`);
};

// Check
logReceipt(
  {descr: 'Burrito', price: 5.99},
  {descr: 'Chips & Salsa', price: 2.99},
  {descr: 'Sprite', price: 1.99}
);

// should log something like:
// Burrito - $5.99
// Chips & Salsa - $2.99
// Sprite - $1.99
// Total - $10.97

// Extra credit: calculate tax

const calculateTax = function calculateTax(price) {
  taxRate = 10.1; // Tax rate in Seattle!
  let tax = price * taxRate;
};

console.log(calculateTax(logReceipt.total));



// New spaceship

const SpaceShip = function SpaceShip(name, topSpeed) {
  const shipName = name;
  let speed = topSpeed;
  this.accelerate = function() {
      console.log(`${name} moving to ${topSpeed}`);
    };
  this.changeSpeed = function(newSpeed) {topSpeed = newSpeed;}
};

let ussEnterprise = new SpaceShip('USS Enterprise', 'Warp 9.75');
ussEnterprise.accelerate();

let newSpeed = 'Warp 9.99'
ussEnterprise.changeSpeed(newSpeed);
ussEnterprise.accelerate();

const serenity = new SpaceShip('Serenity', '400,000 mph');
serenity.accelerate();
