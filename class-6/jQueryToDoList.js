$( document ).ready(function() {
  // If an li element is clicked, toggle the class "done" on the <li>

  const toggleDone = function(e) {
    let $this = $(this);
    $this.toggleClass('done');
  }

  $('li').on('click', toggleDone);

  // If a delete link is clicked, delete the li element / remove from the DOM

  const deleteItem = function(e) {
    let $this = $(this);
    const $deleteElListItem = $this.parent();
    $deleteElListItem.fadeOut("normal", function() {
      $this.remove();
    });
    e.stopPropagation();
  }

  const $deleteEls = $('.delete');
  $deleteEls.on('click', deleteItem);

  // If a "Move to..."" link is clicked, it should move the item to the correct
  // list.  Should also update the working (i.e. from Move to Later to Move to Today)
  // and should update the class for that link.
  // Should *NOT* change the done class on the <li>

  const moveItem = function(e) {
    let $this = $(this);
    const $moveListItem = $this.parent();
    const $listParent = $moveListItem.parent();

    if ($listParent.hasClass('today-list')) {
      $moveListItem.detach();
      const $laterList = $('.later-list')
      $moveListItem.appendTo($laterList);

      $this.removeClass('toLater');
      $this.addClass('toToday');
      $this.html('Move to Today');

      e.stopPropagation();
    } else {
      $moveListItem.detach();
      const $todayList = $('.today-list')
      $moveListItem.appendTo($todayList);

      $this.removeClass('toToday');
      $this.addClass('toLater');
      $this.html('Move to Later');

      e.stopPropagation();
    }
  }

  const $moveEls = $('.move');
  $moveEls.on('click', moveItem);


  // If an 'Add' link is clicked, adds the item as a new list item in correct list
  // addListItem function has been started to help you get going!
  // Make sure to add an event listener to your new <li> (if needed)
  const addListItem = function(e) {
    e.preventDefault();
    let $this = $(this);
    const text = $this.parent().find('input').val();
    const $parentSection = $this.parent().parent();
    const $listSection = $parentSection.find('ul')

    // // Create li tag and span with text containing the user input
    const $newLiItem = $('<li>');
    $newLiItem.on('click', toggleDone);

    const $spanEl = $('<span>');
    $spanEl.text(text);
    $spanEl.appendTo($newLiItem);
    $newLiItem.appendTo($listSection);

    // if the parent section is 'Today', attach the relevant elements & text
    if ($listSection.hasClass('today-list')) {
      const $moveATag = $('<a>');
      $moveATag.addClass('move toLater');
      $moveATag.text('Move to Later');
      $moveATag.appendTo($newLiItem);

      $moveATag.on('click', moveItem);
    } else { // else, attach the opposite elements & text
      const $moveATag = $('<a>');
      $moveATag.addClass('move toToday');
      $moveATag.text('Move to Today');
      $moveATag.appendTo($newLiItem);

      $moveATag.on('click', moveItem);
    }

    const $deleteATag = $('<a>');
    $deleteATag.addClass('delete');
    $deleteATag.text('Delete');
    $deleteATag.appendTo($newLiItem);

    $deleteATag.on('click', deleteItem);

    e.stopPropagation();
  }
  // Add this as a listener to the two Add links

  const $addEls = $('.add-item');
  $addEls.on('click', addListItem);

});
