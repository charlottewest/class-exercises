/**
 * Creates a new Car object
 * @constructor
 * @param {String} model
 */
const Car = function(model) {
  this.model = model;
  this.currentSpeed = 0;
};

Car.prototype.accelerate = function() {
  this.currentSpeed ++;
};

Car.prototype.brake = function() {
  this.currentSpeed --;
};

Car.prototype.toString = function() {
  return `model: ${this.model},
current speed: ${this.currentSpeed}`;
};

const subbie = new Car('Subaru');
subbie.accelerate();
subbie.accelerate();
subbie.accelerate();
subbie.brake();
console.log(subbie.toString()); // will return "model: Subaru, current speed: 2"
