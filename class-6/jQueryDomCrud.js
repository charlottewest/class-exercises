$( document ).ready(function() {
  // Create a new <a> element containing the text "Buy Now!"
  // with an id of "cta" after the last <p>


  // Access (read) the data-color attribute of the <img>,
  // log to the console
  const $img = $('img');
  console.log($img.data('color'));

  // Update the third <li> item ("Turbocharged"),
  // set the class name to "highlight"

  $('li').eq(2).addClass('highlight');

  // Remove (delete) the last paragraph
  // (starts with "Available for purchase now…")

});
