// Math Practice
//1.
Math.ceil(Math.random() * 13);

//2.
const card1 = Math.ceil(Math.random() * 13); // 7
const card2 = Math.ceil(Math.random() * 13); // 4
const card3 = Math.ceil(Math.random() * 13); // 12

let max = Math.max(card1, card2, card3); // 12

// 3. Find the surface area of the 2 pizzas; A = PI * r^2
const radius1 = 13/2 // 6.5
const surfaceArea1 = Math.PI * (radius1 ** 2) // 132.73228961416876

const radius2 = 17/2 // 8.5
const surfaceArea2 = Math.PI * (radius2 ** 2) // 226.98006922186255

// 4. Find cost per inch
const costPerInch1 = 16.99/surfaceArea1 // 0.12800201103580125
const costPerInch2 = 19.99/surfaceArea2 // 0.08806940657181973

// Address Line
// 1.
const firstName = 'Charlotte';
const lastName = 'West';
const streetAddress = '255 S King St';
const city = 'Seattle';
const state = 'WA';
const zipCode = '98104';

let address = `${firstName} ${lastName}\n${streetAddress}\n${city}, ${state} ${zipCode}`;

// 2.
let fullAddress =
'Holly Beckett\n123 Densmore Avenue\nSeattle, WA 98117'

let addressLine1 = fullAddress.split('\n')[0];
let addressLine2 = fullAddress.split('\n')[1];
let addressLine3 = fullAddress.split('\n')[2];

let newFirstName = addressLine1.split(' ')[0];
let newLastName = addressLine1.split(' ')[1];
let newAddress = addressLine2;
let newCity = addressLine3.split(',')[0]
let newState = addressLine3.split(' ')[1]
let newZip = addressLine3.split(' ')[2]

// Dates

// Find the middle date between 1/1/2019 00:00:00 and 4/1/2019 00:00:00
const startDate = new Date(2019, 0, 1);
const endDate = new Date(2019, 3, 1);
let timeDiff = endDate.getTime() - startDate.getTime(); // 7772400000

let halfDiff = timeDiff / 2;
let middleDate = new Date (startDate.getTime() + halfDiff);
//Thu Feb 14 2019 23:30:00 GMT-0800
