/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl
 * @param {Event} submitEvent
 */
const validateTextInput = function(inputEl, submitEvent) {
  const errorEl = inputEl.parentElement.querySelector('.error');

  if (inputEl.value === ''  || inputEl.value.length < 5) {
    const labelEl = inputEl.parentElement.querySelector('label');
    errorEl.innerHTML = `${labelEl.innerText} must be at least 5 chars`;
    inputEl.parentElement.classList.add('invalid')

    submitEvent.preventDefault();
  } else {
    inputEl.parentElement.classList.remove('invalid');
    errorEl.innterHTML = '';
  }
}

const validateEmailInput = function(inputEl, submitEvent) {
  const errorEl = inputEl.parentElement.querySelector('.error');
  const regex = /^\w+@\w+\.[a-z]*$/;

  if (inputEl.value === ''  || regex.test(inputEl.value) === false) {
    const labelEl = inputEl.parentElement.querySelector('label');
    errorEl.innerHTML = `${labelEl.innerText} must be a valid email (ie: foo@bar.com)`;
    inputEl.parentElement.classList.add('invalid')

    submitEvent.preventDefault();
  } else {
    inputEl.parentElement.classList.remove('invalid');
    errorEl.innterHTML = '';
  }
}


const firstNameEl = document.getElementsByTagName('input')[0];
const lastNameEl = document.getElementsByTagName('input')[1];
const emailEl = document.getElementsByTagName('input')[2];

const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function(e) {
      validateTextInput(firstNameEl, e);
      validateTextInput(lastNameEl, e);
      validateEmailInput(emailEl, e);

      e.preventDefault();
    });
