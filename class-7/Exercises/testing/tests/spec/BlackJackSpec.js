describe("Calc Points Functions", function() {
  it("should correctly calculate the points for a given deck", function() {
    const hand1 = [
      {
      val: 10,
      displayVal: 'King',
      suit: 'hearts',
      },
      {
        val: 7,
        displayVal: '7',
        suit: 'spades',
      },
    ];

    const hand2 = [
      {
        val: 11,
        displayVal: 'Ace',
        suit: 'hearts',
      },
      {
        val: 9,
        displayVal: '9',
        suit: 'spades',
      }
    ];

    const hand3 = [
      {
        val: 10,
        displayVal: 'King',
        suit: 'hearts',
      },
      {
        val: 6,
        displayVal: '6',
        suit: 'spades',
      },
      {
        val: 11,
        displayVal: 'Ace',
        suit: 'spades',
      }
    ];

    expect(calcPoints(hand1).total).toEqual(17);
    expect(calcPoints(hand2).total).toEqual(20);
    expect(calcPoints(hand3).total).toEqual(17);

  });
});
