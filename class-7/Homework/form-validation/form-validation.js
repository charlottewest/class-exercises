/**
 * Validates an individual input on form submit
 * @param {HTMLElement} inputEl
 * @param {Event} submitEvent
 */
const validateNameInput = function(inputEl, submitEvent) {
  const errorEl = inputEl.parentElement.querySelector('.error');

  if (inputEl.value === ''  || inputEl.value.length < 3) {
    const labelEl = inputEl.parentElement.querySelector('label');
    errorEl.innerHTML = `${labelEl.innerText} must be at least 3 chars`;
    inputEl.parentElement.classList.add('invalid')

    submitEvent.preventDefault();
  } else {
    inputEl.parentElement.classList.remove('invalid');
    errorEl.innterHTML = '';
  }
}

const validateEmailInput = function(inputEl, submitEvent) {
  const errorEl = inputEl.parentElement.querySelector('.error');
  const regex = /^\w+@\w+\.[a-z]*$/;

  if (inputEl.value === ''  || regex.test(inputEl.value) === false) {
    const labelEl = inputEl.parentElement.querySelector('label');
    errorEl.innerHTML = `${labelEl.innerText} must be a valid email (ie: foo@bar.com)`;
    inputEl.parentElement.classList.add('invalid')

    submitEvent.preventDefault();
  } else {
    inputEl.parentElement.classList.remove('invalid');
    errorEl.innterHTML = '';
  }
}

const validateMessageInput = function(inputEl, submitEvent) {
  const errorEl = inputEl.parentElement.querySelector('.error');

  if (inputEl.value === ''  || inputEl.value.length < 5) {
    const labelEl = inputEl.parentElement.querySelector('label');
    errorEl.innerHTML = `${labelEl.innerText} must be at least 5 chars`;
    inputEl.parentElement.classList.add('invalid')

    submitEvent.preventDefault();
  } else {
    inputEl.parentElement.classList.remove('invalid');
    errorEl.innterHTML = '';
  }
}


// Used https://stackoverflow.com/questions/24875414/addeventlistener-change-and-option-selection
const selectEls = document.getElementsByTagName('select');
for (let i = 0; i < selectEls.length; i++) {
  selectEls[i].addEventListener('click', function(e) {
    const options = document.getElementsByTagName('option');
    const count = options.length;
  });
}

for (let i = 0; i < selectEls.length; i++) {
  selectEls[i].addEventListener('change', function(e) {
    const formEl = document.getElementById('connect-form');
    const formCheck = document.getElementsByClassName('form-check')[0];
    console.log(formCheck);
    const optionSelected = selectEls[i].options[selectEls[i].selectedIndex].value;

    if (optionSelected === 'job-opportunity') {
      // Add job title field
      const jobTitle = document.createElement('div');
      jobTitle.setAttribute(
        'class',
        'form-group'
        );
      const jobTitleLabel = document.createElement('label');
      jobTitleLabel.setAttribute(
        'for',
        'job-title'
        );
      jobTitleLabel.innerHTML = 'Job Title';

      const jobTitleInput = document.createElement('input');
      jobTitleInput.setAttribute(
        'class',
        'form-control validate-input'
        );
      jobTitleInput.setAttribute(
        'type',
        'text'
        );
      jobTitleInput.setAttribute(
        'name',
        'job-title'
        );

      // Add company website field
      const companySite = document.createElement('div');
      companySite.setAttribute(
        'class',
        'form-group'
        );
      const companySiteLabel = document.createElement('label');
      companySiteLabel.setAttribute(
        'for',
        'company-site'
        );
      companySite.innerHTML = 'Company Website';

      const companySiteInput = document.createElement('input');
      companySiteInput.setAttribute(
        'class',
        'form-control validate-input'
        );
      companySiteInput.setAttribute(
        'type',
        'text'
        );
      companySiteInput.setAttribute(
        'name',
        'company'
        );

      // Insert new input fields
      formEl.insertBefore(jobTitle, formCheck);
      jobTitle.appendChild(jobTitleLabel);
      jobTitle.appendChild(jobTitleInput);

      formEl.insertBefore(companySite, formCheck);
      companySite.appendChild(companySiteLabel);
      companySite.appendChild(companySiteInput);
    } else if (optionSelected === 'talk-code') {
      const talkCode = document.createElement('div');
      talkCode.setAttribute(
        'class',
        'form-group'
        );
      const talkCodeLabel = document.createElement('label');
      talkCodeLabel.setAttribute(
        'for',
        'talk-code'
        );
      talkCodeLabel.innerHTML = 'Talk Code';

      const talkCodeInput = document.createElement('input');
      talkCodeInput.setAttribute(
        'class',
        'form-control validate-input'
        );
      talkCodeInput.setAttribute(
        'type',
        'text'
        );
      talkCodeInput.setAttribute(
        'name',
        'job-title'
        );

      // Insert new input field
      formEl.insertBefore(talkCode, formCheck);
      talkCode.appendChild(talkCodeLabel);
      talkCode.appendChild(talkCodeInput);

    }
  });
}

const nameEl = document.getElementsByTagName('input')[0];
const emailEl = document.getElementsByTagName('input')[1];
const messageEl = document.getElementsByTagName('textarea')[0];

const formEl = document.getElementById('connect-form')
    .addEventListener('submit', function(e) {
      validateNameInput(nameEl, e);
      validateEmailInput(emailEl, e);
      validateMessageInput(messageEl, e);

      e.preventDefault();
    });
