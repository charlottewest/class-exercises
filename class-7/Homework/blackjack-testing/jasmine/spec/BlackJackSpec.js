describe("Dealer should draw Functions", function() {
  it("should correctly evaluate if the dealer should draw", function() {
    const hand1 = [
      {
      val: 10,
      displayVal: 'King',
      suit: 'hearts',
      },
      {
        val: 9,
        displayVal: '9',
        suit: 'spades',
      },
    ];

    const hand2 = [
      {
        val: 11,
        displayVal: 'Ace',
        suit: 'hearts',
      },
      {
        val: 6,
        displayVal: '6',
        suit: 'spades',
      }
    ];

    const hand3 = [
      {
        val: 10,
        displayVal: 'King',
        suit: 'hearts',
      },
      {
        val: 7,
        displayVal: '7',
        suit: 'spades',
      }
    ];

    const hand4 = [
      {
        val: 2,
        displayVal: '2',
        suit: 'hearts',
      },
      {
        val: 4,
        displayVal: '4',
        suit: 'spades',
      },
      {
        val: 2,
        displayVal: '2',
        suit: 'hearts',
      },
      {
        val: 5,
        displayVal: '5',
        suit: 'spades',
      }
    ];

    expect(dealerShouldDraw(hand1)).toBe(false);
    expect(dealerShouldDraw(hand2)).toBe(true);
    expect(dealerShouldDraw(hand3)).toBe(false);
    expect(dealerShouldDraw(hand4)).toBe(true);


  });
});
