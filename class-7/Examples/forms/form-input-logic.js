const formSelect = document.querySelector('select');
const businessEls = document.getElementsByClassName('business');
const technicalEls = document.getElementsByClassName('technical');

const removeClassFromCollection = function(collection, className) {
  for(let i = 0; i < collection.length; i++) {
    collection[i].classList.remove(className)
  }
}

const addClassToCollection = function(collection, className) {
  for(let i = 0; i < collection.length; i++) {
    collection[i].classList.add(className)
  }
}

formSelect.addEventListener('change', function(e) {
  if (this.value === 'business') {
    removeClassFromCollection(businessEls, 'd-none');
    addClassToCollection(technicalEls, 'd-none');

  } else {
    removeClassFromCollection(technicalEls, 'd-none');
    addClassToCollection(businessEls, 'd-none');
  }

});