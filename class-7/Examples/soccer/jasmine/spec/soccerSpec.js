describe("Soccer Functions", function() {
  describe("When results are passed as a string to getTotalPoints", function() {
    it("should correctly calculate the points", function() {
      expect(soccer.getTotalPoints('wwdl')).toEqual(7);
      expect(soccer.getTotalPoints('wlldwddldw')).toEqual(13);
    });
  });

  describe("When orderTeams is called", function() {
    let sounders = {name: 'Sounders', results: 'wwdl'};
    let galaxy = {name: 'Galaxy', results: 'wlld'};

    it("should call getTotalPoints function", function() {
      spyOn(soccer, 'getTotalPoints');

      soccer.orderTeams(sounders, galaxy);

      expect(soccer.getTotalPoints).toHaveBeenCalled();
      expect(soccer.getTotalPoints).toHaveBeenCalledTimes(2);
      expect(soccer.getTotalPoints).toHaveBeenCalledWith('wwdl');
      expect(soccer.getTotalPoints).toHaveBeenCalledWith('wlld');
    });

    it("should contain correct info", function() {
      const standings = soccer.orderTeams(sounders, galaxy);
      expect(standings).toContain('Sounders: 7');
      expect(standings).toContain('Galaxy: 4');
    });
  });
});
