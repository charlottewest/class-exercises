// let movieTimesPromise = // promise
// movieTimesPromise.then(function(movieTimes) {
//   // Add movie time info to page
// });
// movieTimesPromise.catch(function() {
//   // Display error message to user
// })

const movieDiv = document.getElementsByTagName('body')[0];
let movieTimesPromise = new Promise(function(resolve, reject) {
  setTimeout(function() {
    const movieTime = {
      name: 'Captain Marvel',
      time: '19:20',
      location: 'Meridian 16'
    };
    resolve(movieTime)
  }, 1500);
})
movieTimesPromise.then(function(movieTime) {
  movieDiv.innerHTML = `
    <h1>${movieTime.name}</h1>
    <h2><em>${movieTime.location}</em> ${movieTime.time}</h2>
  `;
});
movieTimesPromise.catch(function(e) {
  movieDiv.innerHTML = "Unable to get movie times";
});
