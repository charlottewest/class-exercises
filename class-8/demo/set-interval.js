const timeLeft = {
  hours: 14,
  minutes: 2,
  seconds: 12,
  decreaseSeconds: function() {
    if (this.seconds === 0) {
      this.seconds = 59;
      if (this.minutes === 0) {
        this.hours--;
        this.minutes = 59;
      } else {
        this.minutes--;
      }
    } else {
      this.seconds--;
    }
  }
};

const updateTime = function(timeLeft) {
  document.getElementById('hours').innerText = timeLeft.hours;
  document.getElementById('minutes').innerText = timeLeft.minutes;
  document.getElementById('seconds').innerText = timeLeft.seconds;
}
updateTime(timeLeft);

// Change time remaining every 1 second
let countdownInterval = setInterval(
  function() {
    if (timeLeft.hours === 0 && timeLeft.minutes === 0 && timeLeft.seconds === 0) {
      clearInterval(countdownInterval);
    } else {
      timeLeft.decreaseSeconds();
      updateTime(timeLeft);
    }
  },
  1000
);

// // Alternative using only setTimeout
// let countdown = function() {
//   if (timeLeft.hours === 0 && timeLeft.minutes === 0 && timeLeft.seconds === 0) {
//     return;
//   }
//
//   timeLeft.decreaseSeconds();
//   updateTime(timeLeft);
//   setTimeout(countdown, 1000);
// };
//
// setTimeout(countdown, 1000);
