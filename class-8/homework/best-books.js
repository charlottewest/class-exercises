// create api-key.js file with const API_KEY="your_api_key" in this same directory to use
const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists';
const DATE_URL = '/2019-01-20/hardcover-fiction.json';

const formEl = document.getElementById('book-search-form')
formEl.addEventListener('submit', function(e) {
  e.preventDefault();
  const yearVal = document.getElementsByTagName('input')[0].value;
  const monthVal = document.getElementsByTagName('input')[1].value;
  const dayVal = document.getElementsByTagName('input')[2].value;

  dateVal = `${yearVal}-${monthVal}-${dayVal}`
  console.log(dateVal);
  const url = `${BASE_URL}/${dateVal}/hardcover-fiction.json?api-key=${API_KEY}`;
  fetch(url)
    .then(function(response) {
      return response.json();
    })
    .then(function(responseJson) {
      console.log(responseJson);

      //window.responseJson = responseJson;
      const books = responseJson.results.books;
      const bookEls = document.getElementsByClassName('book');

      for (let i = 0; i < 5; i++) {
        const bookTitle = books[i].title;
        bookEls[i].getElementsByClassName('book-title')[0].innerText = bookTitle;

        const bookAuthor = books[i].author;
        bookEls[i].getElementsByClassName('book-author')[0].innerText = bookAuthor;

        const bookDescr = books[i].description;
        bookEls[i].getElementsByClassName('book-descr')[0].innerText = bookDescr;

        const imgUrl = books[i].book_image;
        bookEls[i].getElementsByClassName('book-img')[0].src = imgUrl;
      }
    });
});
