let colorVal = 0;
const interval = setInterval(
  function() {
    colorVal++;
    document.body.style.backgroundColor = `rgb(${colorVal},${colorVal},${colorVal})`;

      if(colorVal === 255) {
        clearInterval(interval);
      }
    },
  50
);

let textColorVal = 255;
const textColorInterval = setInterval(
  function() {
    textColorVal--;
    const headerEl = document.getElementById('header-el');
    headerEl.style.color = `rgb(${textColorVal},${textColorVal},${textColorVal})`;

      if(textColorVal === 0) {
        clearInterval(textColorInterval);
      }
    },
  50
);
