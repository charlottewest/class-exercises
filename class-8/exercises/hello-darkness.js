let colorVal = 255;
const interval = setInterval(
  function() {
    colorVal--;
    document.body.style.backgroundColor = `rgb(${colorVal},${colorVal},${colorVal})`;

      if(colorVal === 0) {
        clearInterval(interval);
      }
    },
  50
);


// const darkenBody = function() {
//   colorVal--;
//   document.body.style.backgroundColor = `rgb(${colorVal},${colorVal},${colorVal})`;
//   console.log(`rgb(${colorVal},${colorVal},${colorVal})`);
//   if(colorVal === 0) {
//     clearInterval(interval);
//   }
// }

// const interval = setInterval(darkenBody, 50);
