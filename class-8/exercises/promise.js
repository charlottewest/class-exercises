// – After 1 second, call Math.random()
// – If the result of Math.random() is > 0.5, call resolve()
// – If the result of Math.random() is <= 0.5, call reject()

let randNumPromise = new Promise(function(resolve, reject) {
  setTimeout(function() {
    if(Math.random() > 0.5) {
      resolve();
    } else {
      reject();
    }
  }, 1000);
});

randNumPromise
  .then(function() {
    console.log('success');
  })
  .catch(function() {
    console.log('fail');
  })
  .then(function() {
    console.log('complete');
  });
